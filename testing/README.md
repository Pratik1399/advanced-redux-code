##Project Generation

npm install -g create-react-app
create-react-app testing

##Our First Test

npm run test

##Install Dependencies

npm install --save redux react-redux

##Rendering app

npm run start

##Enzyme Setup

npm install --save enzyme enzyme-adapter-react-16

##One More Feature

npm install --save axios redux-promise moxios

##Adding React Router

npm install --save react-router-dom